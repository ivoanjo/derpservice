plugins {
    id("org.jetbrains.kotlin.jvm").version("1.3.31")
    id("com.github.johnrengelman.shadow").version("5.0.0")
    application
}

repositories {
    jcenter()
}

dependencies {
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")

    implementation("org.http4k:http4k-core:3.154.0")
    implementation("org.http4k:http4k-serverless-lambda:3.154.0")
    //testImplementation("org.http4k:http4k-client-okhttp:3.154.0")
}

application {
    mainClassName = "derpservice.AppKt"
}
