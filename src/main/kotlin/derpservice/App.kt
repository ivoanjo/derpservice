package derpservice

import org.http4k.core.Filter
import org.http4k.core.HttpHandler
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.core.then
import org.http4k.filter.CachingFilters
import org.http4k.routing.bind
import org.http4k.routing.path
import org.http4k.routing.routes
import org.http4k.server.SunHttp
import org.http4k.server.asServer
import org.http4k.serverless.AppLoader

class DerpService {
    val derpApp: HttpHandler

    init {
        val app: HttpHandler = routes(
            "/ping" bind GET to { _: Request -> Response(OK).body("pong!") },
            "/greet/{name}" bind GET to { req: Request ->
                val path: String? = req.path("name")
                Response(OK).body("hello ${path ?: "anon!"}")
            },
            "/greet" bind GET to { _: Request ->
                Response(OK).body("howdy!")
            }
        )

        // this is a Filter - it performs pre/post processing on a request or response
        val timingFilter = Filter {
            next: HttpHandler ->
            {
                request: Request ->
                val start = System.currentTimeMillis()
                val response = next(request)
                val latency = System.currentTimeMillis() - start
                println("Request to ${request.uri} took ${latency}ms")
                response
            }
        }

        // we can "stack" filters to create reusable units, and then apply them to an HttpHandler
        val compositeFilter = CachingFilters.Response.NoCache().then(timingFilter)
        val filteredApp: HttpHandler = compositeFilter.then(app)

        derpApp = filteredApp
    }
}

// Entry point used for Lambda
object DerpServiceLambda : AppLoader {
    override fun invoke(env: Map<String, String>): HttpHandler = DerpService().derpApp
}

// Entry point used for local testing
fun main(args: Array<String>) {
    val port = 9000
    println("Running on http://localhost:$port")
    DerpService().derpApp.asServer(SunHttp(port)).start()
}
